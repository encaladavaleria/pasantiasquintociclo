<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Rastreo de Personas</title>
        <link rel="shortcut icon" href="img/icono1.png?1.1.9.25">
        <link rel="stylesheet" type="text/css" href="extjs/build/classic/theme-crisp/resources/theme-crisp-all.css">
        <link rel="stylesheet" type="text/css" href="extjs/build/packages/font-awesome/resources/font-awesome-all.css">        <script type="text/javascript" src="extjs/ext-all.js"></script>
        

        <script type="text/javascript">
//            function login(form, valueMpcPant) {
//                if (form.isValid()) {
//                    form.submit({
//                        waitTitle: 'Información',
//                        waitMsg: 'Verificando...',
//                        success: function (form, action) {
//                            var record = action.result.data;
//                            Ext.create('Ext.form.Panel').getForm().submit({
//                                //url: 'php/login/login.php',
////                                params: {
//////                                    idPersona: record.idPersonal,
////                                    mpcPant: valueMpcPant,
//////                                    usuario: record.persona,
////                                    idRolUsuario: record.rol,
////                                    idPersona: 1,
////                                    usuario: record.user
////                                },
//                                success: function () {
//                                    mensajeAlerta('Usuario Correcto');
//                                }
//                            });
//                        },
//                        failure: function (form, action) {
//                            switch (action.failureType) {
//                                case Ext.form.action.Action.CONNECT_FAILURE:
//                                    mensajeError(MENSAJE_ERROR);
//                                    break;
//                                case Ext.form.action.Action.SERVER_INVALID:
//                                    mensajeAlerta(action.result.message);
//                                    break;
//                            }
//                        }
//                    });
//                }
//            }

            function mensajeAlerta(mensaje) {
                Ext.MessageBox.show({
                    title: 'Alerta',
                    msg: mensaje,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.WARNING
                });
            }

            function mensajeError(mensaje) {
                Ext.MessageBox.show({
                    title: 'Error',
                    msg: mensaje,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.ERROR
                });
            }

            Ext.define("Ext.theme.neptune.panel.Panel", {
                override: "Ext.panel.Panel",
                border: false,
                bodyBorder: false,
                initBorderProps: Ext.emptyFn,
                initBodyBorder: function () {
                    if (this.bodyBorder !== true) {
                        this.callParent();
                    }
                }});

            Ext.onReady(function () {
                Ext.create('Ext.container.Viewport', {
                    layout: 'border',
                    items: [{
                            xtype: 'image',
                            src: 'img/mapa.png',
                            plugins: 'responsive',
                            responsiveConfig: {
                                'width >= 800': {
                                    padding: '0 0 0 0'
                                }
                            }
                        }]
                });

                Ext.create('Ext.window.Window', {
                    layout: 'fit',
                    title: '<b>INICIAR SESIÓN</b>',
                    iconCls: 'x-fa fa-user',
                    resizable: false,
                    closable: false,
                    height: 175,
                    closeAction: 'hide',
                    plugins: 'responsive',
                    style: {
                        borderColor: 'grey'
                    },
                    responsiveConfig: {
                        'width >= 800': {
                            width: 350
                        },
                        'width < 800': {
                            width: 250
                        }
                    },
                    items: [{
                            xtype: 'form',
                            defaultType: 'textfield',
                            bodyPadding: 10,
//                            url: SERVICIO_WEB + 'com.kradac.pantallas.entities.usuarios/login',
                           // url: 'php/login/prueba.php',
                            plugins: 'responsive',
                            responsiveConfig: {
                                'width >= 800': {
                                    defaults: {
                                        labelWidth: 125,
                                        anchor: '100%',
                                        allowBlank: false,
                                      //  blankText: INFO_MESSAGE_BLANK_TEXT
                                    }
                                },
                                'width < 800': {
                                    defaults: {
                                        anchor: '100%',
                                        allowBlank: false,
                                      //  blankText: INFO_MESSAGE_BLANK_TEXT
                                    }
                                }
                            },
                            items: [{
                                    name: 'usuario',
                                    plugins: 'responsive',
                                    id:'nombreUsuario',
                                    responsiveConfig: {
                                        'width >= 800': {
                                            fieldLabel: '<b>Usuario</b>'
                                        },
                                        'width < 800': {
                                            emptyText: 'Usuario'
                                        }
                                    },
                                    listeners: {
                                        specialkey: function (field, e) {
                                            if (e.getKey() === e.ENTER) {
                                                login(this.up('form').getForm(), this.up('form').getForm().findField('clave').value);
                                            }
                                        }
                                    }
                                }, {
                                    name: 'clave',
                                    inputType: 'password',
                                    plugins: 'responsive',
                                    id:'claveUsuario',
                                    responsiveConfig: {
                                        'width >= 800': {
                                            fieldLabel: '<b>Contraseña</b>'
                                        },
                                        'width < 800': {
                                            emptyText: 'Contraseña'
                                        }
                                    },
                                    listeners: {
                                        specialkey: function (field, e) {
                                            if (e.getKey() === e.ENTER) {
                                               // login(this.up('form').getForm(), this.up('form').getForm().findField('clave').value);
                                            }
                                        }
                                    }
                                }, {
                                    xtype: 'panel',
                                    layout: {
                                        type: 'vbox',
                                        align: 'center'
                                    },
                                    items: [{
                                            xtype: 'checkbox',
                                            boxLabel: 'Mostrar Contraseña',
                                            listeners: {
                                                change: function (thisObj, newValue, oldValue, eOpts) {
                                                    if (newValue) {
                                                        this.up('form').getForm().findField('clave').inputEl.dom.type = 'text';
                                                    } else {
                                                        this.up('form').getForm().findField('clave').inputEl.dom.type = 'password';
                                                    }
                                                }
                                            }
                                        }]
                                }],
                            buttonAlign: 'center',
                            buttons: [{
                                    text: '<b>Ingresar</b>',
                                    iconCls: 'x-fa fa-sign-in',
                                    formBind: true, //only enabled once the form is valid
                                    disabled: true,
                                    handler: function () {
                                        mensajeAlerta('hola '+Ext.getCmp('nombreUsuario').getValue());
                                        //location.href="php/mapa/indexMapa.php";
                                        window.open("php/mapa/indexMapa.php");
//                                        console.log('Clave: ' + this.up('form').getForm().findField('clave').value);
                                       // login(this.up('form').getForm(), this.up('form').getForm().findField('clave').value);
                                    }
                                }, {
                                    text: '<b>Cancelar</b>',
                                    iconCls: 'fa fa-times',
                                    handler: function () {
                                        location.href = 'index.php';//poner una enlace para llamar a pagina cliente
                                    }
                                }]
                        }]
                }).show();
            });
        </script>
    </head>

</body>
</html>
