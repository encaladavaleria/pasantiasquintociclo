Ext.onReady(function () {
    Ext.create('Ext.container.Viewport', {
        layout: 'border',
        items: [{
                region: 'center',
                //title: 'Geolocalización',
                id: 'map'
            }, {
                region: 'west',
                width: '20%',
                xtype: 'panel',
                title: 'Historial',
                collapsed: true,
                collapsible: true,
                items: [{
                        xtype: 'grid',
                        //title: 'Grupos',
                        padding: '0 10 0 10',
                        height: 400,
                        scrollable: true,
                        hideHeaders: true,
                        viewConfig: {
                            preserveScrollOnReload: true,
                            enableTextSelection: true,
                            loadMask: false
                        },
                        columns: [
                            {
                                menuDisabled: true,
                                xtype: 'actioncolumn',
                                width: 30,
                                items: [{
                                        iconCls: 'x-fa fa-trash',
                                        tooltip: 'Eliminar Usuario',
                                        handler: function (grid, rowIndex, colIndex) {
                                            Ext.Msg.show({
                                                title: 'Eliminar Usuario',
                                                scrollable: false,
                                                message: '¿Esta seguro que desea eliminar el usuario?<br><br>',
                                                buttons: Ext.Msg.YESNO,
                                                icon: Ext.Msg.QUESTION,
                                                height: 127,
                                                buttonText: {
                                                    yes: 'Si'
                                                },
                                                fn: function (btn) {
                                                    if (btn === 'yes') {
                                                        var rec = grid.getStore().getAt(rowIndex);
                                                        Ext.create('Ext.form.Panel').getForm().submit({
                                                            url: SERVICIO_WEB + 'com.kradac.pantallas.entities.usuarios/eliminar',
                                                            params: {
                                                                cpmPant: ABC_ONIX,
                                                                mpcPant: CBA_ONIX,
                                                                idUsuario: rec.id
                                                            },
                                                            success: function (form, action) {
                                                                storePersonal.reload();
                                                                mensajeInformativoEfecto(action.result.message);
                                                            },
                                                            failure: function (form, action) {
                                                                storePersonal.rejectChanges();
                                                                mensajeAlerta(action.result.message);
                                                            }
                                                        });
                                                    }
                                                }
                                            });
                                        }
                                    }]
                            },
                            {dataIndex: 'Ubicación', width: 185, cellWrap: true, filter: true, flex: 2, tdCls: 'x-change-cell'}
                        ]
                    }]
            }]
    });
    initMap();
    function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: -34.397, lng: 150.644},
            zoom: 18,
            mapTypeId: google.maps.MapTypeId.HYBRID
        });
        var infoWindow = new google.maps.InfoWindow({map: map});

        // Try HTML5 geolocation.
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };

                infoWindow.setPosition(pos);
                infoWindow.setContent('Ubicación Actual');
                map.setCenter(pos);
            }, function () {
                handleLocationError(true, infoWindow, map.getCenter());
            });
        } else {
            // Browser doesn't support Geolocation
            handleLocationError(false, infoWindow, map.getCenter());
        }
    }

    function handleLocationError(browserHasGeolocation, infoWindow, pos) {
        infoWindow.setPosition(pos);
        infoWindow.setContent(browserHasGeolocation ?
                'Error: The Geolocation service failed.' :
                'Error: Your browser doesn\'t support geolocation.');
    }
});